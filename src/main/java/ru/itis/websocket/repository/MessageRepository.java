package ru.itis.websocket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.websocket.model.Message;

public interface MessageRepository extends JpaRepository<Message, Long> {

}
