package ru.itis.websocket.service;

import org.springframework.stereotype.Service;
import ru.itis.websocket.jwt.JwtManager;
import ru.itis.websocket.model.User;
import ru.itis.websocket.repository.UserRepository;

import java.util.Optional;

@Service
public class UserService {

    private UserRepository userRepository;
    private JwtManager jwtManager;

    public UserService(UserRepository userRepository, JwtManager jwtManager) {
        this.userRepository = userRepository;
        this.jwtManager = jwtManager;
    }

    public String login(String username, String password) {
        Optional<User> candidate = userRepository.getByUsername(username);
        if (candidate.isPresent()) {
            User user = candidate.get();
            if (password.equals(user.getPassword())) {
                return jwtManager.createToken(user.getUsername(), user.getId());
            }
        }
        throw new IllegalArgumentException("Login attempt failed");
    }

    public String register(User userToSave) {
        if (!userRepository.existsByUsername(userToSave.getUsername())) {
            User user = userRepository.save(userToSave);
            return jwtManager.createToken(user.getUsername(), user.getId());
        }
        throw new IllegalArgumentException("Register attempt failed");
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
}
