package ru.itis.websocket.dto;

import lombok.Data;

@Data
public class UserDto {
    private String username;
    private String password;
}
