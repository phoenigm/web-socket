package ru.itis.websocket.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import ru.itis.websocket.dto.MessageDto;
import ru.itis.websocket.jwt.JwtManager;
import ru.itis.websocket.model.Message;
import ru.itis.websocket.service.MessageService;
import ru.itis.websocket.service.UserService;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class MessagesWebSocketHandler extends TextWebSocketHandler {
    private static Map<String, WebSocketSession> sessions = new ConcurrentHashMap<>();

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private JwtManager jwtManager;

    @Autowired
    private MessageService messageService;

    @Autowired
    private UserService userService;

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        String messageAsString = (String) message.getPayload();
        MessageDto body = objectMapper.readValue(messageAsString, MessageDto.class);
        String token = body.getFrom();
        String username = jwtManager.getUsername(token);

        sessions.putIfAbsent(body.getFrom(), session);
        Message messageFromDb = messageService.saveMessage(Message.builder()
                .sender(userService.findByUsername(username))
                .text(body.getText())
                .build());

        for (WebSocketSession currentSession : sessions.values()) {
            currentSession.sendMessage(new TextMessage(objectMapper.writeValueAsString(messageFromDb)));
        }
    }
}