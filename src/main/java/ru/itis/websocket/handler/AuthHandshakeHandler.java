package ru.itis.websocket.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeFailureException;
import org.springframework.web.socket.server.HandshakeHandler;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;
import org.springframework.web.util.WebUtils;
import ru.itis.websocket.jwt.JwtManager;

import javax.servlet.http.Cookie;
import java.util.Map;

@Component
public class AuthHandshakeHandler implements HandshakeHandler {

    private JwtManager jwtManager;

    private DefaultHandshakeHandler handshakeHandler = new DefaultHandshakeHandler();

    public AuthHandshakeHandler(JwtManager jwtManager) {
        this.jwtManager = jwtManager;
    }

    @Override
    public boolean doHandshake(ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse, WebSocketHandler webSocketHandler, Map<String, Object> map) throws HandshakeFailureException {
        ServletServerHttpRequest request = (ServletServerHttpRequest)serverHttpRequest;
        Cookie cookie = WebUtils.getCookie(request.getServletRequest(), "AUTH");
        String token = "";
        if (cookie != null) {
            token = cookie.getValue();
        }
        if (jwtManager.validateToken(token)) {
            return handshakeHandler.doHandshake(serverHttpRequest, serverHttpResponse, webSocketHandler, map);
        } else {
            serverHttpResponse.setStatusCode(HttpStatus.FORBIDDEN);
            return false;
        }

/*
        String token = jwtHelper.resolveToken(request.getServletRequest());
        if (jwtHelper.validateToken(token)) {
            return handshakeHandler.doHandshake(serverHttpRequest, serverHttpResponse, webSocketHandler, map);
        } else {
            serverHttpResponse.setStatusCode(HttpStatus.FORBIDDEN);
            return false;
        }
*/

    }
}
